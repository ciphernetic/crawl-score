var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/score/:user', function(req, res){

	url = 'http://crawl.akrasiac.org/scoring/players/' + req.params.user + '.html';
				
	request(url, function(error, responseCode, html) {
		if(!error)
		{
			var $ = cheerio.load(html);
			var wins = [];
			
			var winTable = $('.game_table').filter(function(i, e) {
					return $(this).children('h3').text() === "Wins";
				});

			// only want to look in the first table.
			$('.win', winTable).each(function(i, w) {
				var children = $(this).children('td');
				
				wins.push({order : parseInt($(children[0]).text(), 10),
					score: $(children[1]).text(),
					combo: $(children[2]).text(),
					race: $(children[2]).text().slice(0, 2),
					background: $(children[2]).text().slice(2, 4),
					title: $(children[3]).text(),
					message: $(children[4]).text(),
					turns: parseInt($(children[5]).text(), 10),
					totalTime: $(children[6]).text(),
					god: $(children[7]).text(),
					runes: parseInt($(children[8]).text(), 10),
					morgue: $(children[9]).text(),
					version: $(children[10]).text(),
				});
			});


		}
		else
		{
			console.log(error);
		}
		
		res.json(wins);
	});
});

app.listen(process.env.PORT);
//app.listen('8081');

console.log('server started');
